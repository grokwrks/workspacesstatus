let AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });

let cw = new AWS.CloudWatch({ apiVersion: '2010-08-01' });
let ws = new AWS.WorkSpaces({ apiVersion: '2015-04-08' });

let listparams = {
    MetricName: 'UserConnected',
    Namespace: 'AWS/WorkSpaces',
};

exports.handler = async(event) => {

    let output = [];

    try {

        let workspaceinfo;
        let workspaceconnection;

        let results = await Promise.all([ws.describeWorkspaces().promise(), ws.describeWorkspacesConnectionStatus().promise()]);


        await results.map(function(value) {
            if (value.Workspaces) {
                workspaceinfo = value.Workspaces;
            }
            else if (value.WorkspacesConnectionStatus) {
                workspaceconnection = value.WorkspacesConnectionStatus;
            }
        });

        workspaceinfo.forEach(function(element) {
            let id = element.WorkspaceId;
            let user = element.UserName;
            let connected;
            let lastCheck;
            let lastConnect;

            for (let i = 0; i < workspaceconnection.length; i++) {
                let value = workspaceconnection[i];
                if (value.WorkspaceId == id) {
                    if (value.ConnectionState == "CONNECTED") {
                        connected = true;
                    }
                    else {
                        connected = false;
                    }
                    lastCheck = value.ConnectionStateCheckTimestamp;
                    lastConnect = value.LastKnownUserConnectionTimestamp;
                    workspaceconnection.splice(i, 1);
                    break;
                }
            }

            output.push({ "User": user, "Connected": connected, "Checked": lastCheck, "LastConnected": lastConnect });
        });

        return output;

    }
    catch (err) {
        console.log(err);
        return (err);
    }

};

